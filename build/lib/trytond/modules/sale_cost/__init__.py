# This file is part of Tryton.  The COPYRIGHT file at the top level of this
#repository contains the full copyright notices and license terms.
from trytond.pool import Pool
from . import sale
from . import product


def register():
    Pool.register(
        # sale.Sale,
        # product.Template,
        # sale.SaleProfitJournalStart,
        module='sale_cost', type_='model')
    Pool.register(
        # sale.SaleProfitJournalReport,
        module='sale_cost', type_='report')
    Pool.register(
        # sale.SaleProfitJournal,
        module='sale_cost', type_='wizard')
