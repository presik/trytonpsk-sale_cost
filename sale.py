# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from datetime import date
from decimal import Decimal
from trytond.model import fields, ModelView
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval
from trytond.report import Report
from trytond.wizard import Wizard, StateView, Button, StateReport
from trytond.transaction import Transaction

STATES = {
        'readonly': Eval('state').in_(['confirmed', 'processing', 'done']),
        'required': Eval('state') == 'quotation',
}

_ZERO = Decimal(0)


class Sale(metaclass=PoolMeta):
    __name__ = 'sale.sale'
    cost = fields.Function(fields.Numeric('Sale Cost',
            digits=(16,2)), 'get_sale_cost')
    profit_amount = fields.Function(fields.Numeric('Profit Amount',
            digits=(16,2)), 'get_profit_amount')

    def get_sale_cost(self, name=None):
        res = []
        for line in self.lines:
            if line.type != 'line':
                return res
            expense = line.product.template.expense or Decimal('0.0')
            res.append((line.product.template.cost_price + expense) * Decimal(line.quantity))
        return sum(res)

    def get_profit_amount(self, name=None):
        res = self.total_amount - self.get_sale_cost()
        return res


class SaleProfitJournalStart(ModelView):
    'Sale Profit Journal Start'
    __name__ = 'sale.profit_journal.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    sale_date = fields.Date('Sale Date', required=True)

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_sale_date():
        return date.today()


class SaleProfitJournal(Wizard):
    'Sale Profit Journal'
    __name__ = 'sale.profit_journal'
    start = StateView('sale.profit_journal.start',
        'sale_cost.sale_profit_journal_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
            ])
    print_ = StateReport('sale_cost.profit_journal_report')

    def do_print_(self, action):
        data = {
            'company': self.start.company.id,
            'sale_date': self.start.sale_date,
            }
        return action, data

    def transition_print_(self):
        return 'end'


class SaleProfitJournalReport(Report):
    __name__ = 'sale_cost.profit_journal_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        Sale = pool.get('sale.sale')
        Tax = pool.get('account.tax')
        Company = pool.get('company.company')

        def compute_amount_with_tax(line):
            tax_list = Tax.compute(line.product.supplier_taxes_used,
                line.product.cost_price or Decimal('0.0'),
                line.quantity or 0.0)
            tax_amount = sum([t['amount'] for t in tax_list], Decimal('0.0'))
            amount = line.amount
            return (line.amount if amount else _ZERO) + tax_amount

        total_profit = _ZERO
        sales = Sale.search([
            ('sale_date', '=', data['sale_date']),
            ('state', 'in', ['processing', 'done', 'confirmed']),
        ], order=[('sale_date', 'ASC')])

        total_profit = []
        untaxed_amount = []
        tax_amount = []
        total_amount = []
        for sale in sales:
            total_profit.append(sale.profit_amount)
            untaxed_amount.append(sale.untaxed_amount)
            tax_amount.append(sale.tax_amount)
            total_amount.append(sale.total_amount)
        report_context['records'] = sales
        report_context['company'] = Company(data['company'])
        report_context['tax_amount'] = sum(tax_amount)
        report_context['untaxed_amount'] = sum(untaxed_amount)
        report_context['total_amount'] = sum(total_amount)
        report_context['total_profit'] = sum(total_profit)
        report_context['date'] = data['sale_date']
        return report_context
